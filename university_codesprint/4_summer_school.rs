use std::io;

fn howManyStudents(m: usize, n: usize, c: Vec<usize>) -> Vec<u64> {
    let mut s = Vec::new();

    for _ in 0..m {
        s.push(0);
    }

    for i in 0..n {
        s[c[i]] = s[c[i]] + 1;
    }

    s
}

fn main() {
    let mut input = String::new();
    io::stdin()
        .read_line(&mut input)
        .expect("Number of student and class");
    let mut _iter = input.split_whitespace();
    let n = _iter
        .next()
        .unwrap()
        .parse::<usize>()
        .expect("Enter number of student");
    let m = _iter
        .next()
        .unwrap()
        .parse::<usize>()
        .expect("Enter number of class");

    let mut cinput = String::new();
    io::stdin()
        .read_line(&mut cinput)
        .expect("Number of student and class");
    let mut citer = cinput.split_whitespace();
    let mut c = Vec::new();
    for _ in 0..n {
        let i = citer
            .next()
            .unwrap()
            .parse::<usize>()
            .expect("Enter class index");
        c.push(i);
    }

    let s = howManyStudents(m, n, c);
    for i in 0..m {
        print!("{} ", s[i]);
    }
}
