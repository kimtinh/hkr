use std::io;

fn largest_prime_factor(n: u64) -> u64 {
    let mut a = n;
    let mut r = 1;
    while a % 2 == 0 {
        a /= 2;
        r = 2;
    }
    let mut f = 3;
    let sqrt = (n as f64).sqrt() as u64;
    while f <= sqrt {
        while a % f == 0 {
            a /= f;
            r = f;
        }
        f += 2;
    }

    if a == 1 {
        return r;
    } else {
        return a;
    }
}

fn main() {
    let mut nkinput = String::new();
    io::stdin().read_line(&mut nkinput).expect("Enter number");
    let t = nkinput
        .trim()
        .parse::<usize>()
        .expect("Please enter number");

    let mut n = Vec::new();
    for _ in 0..t {
        let mut input = String::new();
        io::stdin().read_line(&mut input).expect("Enter number");
        let _n = input.trim().parse::<u64>().expect("Please enter number");
        n.push(_n);
    }

    for j in 0..t {
        let a = n[j];
        println!("{}", largest_prime_factor(a));
    }
}
