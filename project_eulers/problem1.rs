use std::io;

fn sum_div_by(n: u64, d: u64) -> u64 {
    let p = (n - 1) / d;
    return d * (p * (p + 1) / 2);
}

fn main() {
    let mut nkinput = String::new();
    io::stdin().read_line(&mut nkinput).expect("Enter number");
    let t = nkinput
        .trim()
        .parse::<usize>()
        .expect("Please enter number");

    let mut n = Vec::new();
    for _ in 0..t {
        let mut input = String::new();
        io::stdin().read_line(&mut input).expect("Enter number");
        let _n = input.trim().parse::<u64>().expect("Please enter number");
        n.push(_n);
    }

    for j in 0..t {
        let a = n[j];
        println!(
            "{}",
            sum_div_by(a, 3) + sum_div_by(a, 5) - sum_div_by(a, 15)
        );
    }
}
