use std::io;

fn sum_even_fibo(n: u64) -> u64 {
    let mut a = 1;
    let mut b = 1;
    let mut c = a + b;
    let mut sum = 0;
    while c < n {
        sum += c;
        a = b + c;
        b = c + a;
        c = a + b;
    }

    return sum;
}

fn main() {
    let mut nkinput = String::new();
    io::stdin().read_line(&mut nkinput).expect("Enter number");
    let t = nkinput
        .trim()
        .parse::<usize>()
        .expect("Please enter number");

    let mut n = Vec::new();
    for _ in 0..t {
        let mut input = String::new();
        io::stdin().read_line(&mut input).expect("Enter number");
        let _n = input.trim().parse::<u64>().expect("Please enter number");
        n.push(_n);
    }

    for j in 0..t {
        let a = n[j];
        println!("{}", sum_even_fibo(a));
    }
}
