use std::io;

fn checkAll(n: usize, h: Vec<i64>, p: Vec<i64>) -> String {
    let mut left = false;
    let mut right = false;
    let mut a = 0;

    let mut max = 0;
    while a <= max {
        let mut i = a;
        loop {
            if p[i] + h[i] >= p[n - 1] {
                left = true;
                break;
            }

            let mut check = true;
            if p[i] + h[i] < p[i + 1] {
                break;
            } else if max < i + 1{
                max = i + 1;
            }
            for j in 2..(n - i) {
                if i + j < n && p[i] + h[i] < p[i + j] {
                    i = i + j - 1;
                    check = false;
                    break;
                } else if max < i + j{
                    max = i + j;
                }
            }
            if check {
                break;
            }
        }
        if left {
            break;
        }
        a = a + 1;
    }

    let mut min = n - 1;
    a = n - 1;
    while a >= min {
        let mut i = a;
        loop {
            if p[i] - h[i] <= p[0] {
                right = true;
                break;
            }

            let mut check = true;
            if p[i] - h[i] > p[i - 1] {
                break;
            } else if min > i - 1{
                min = i - 1;
            }

            for j in 2..(i + 1) {
                if i >= j && p[i] - h[i] > p[i - j] {
                    i = i - j + 1;
                    check = false;
                    break;
                }else if min > i - j {
                    min = i - j;
                }
            }
            if check {
                break;
            }
        }
        if right{
            break;
        }
        if a > 1 {
            a = a - 1;
        } else {
            break;
        }
    }

    if left && right {
        return String::from("BOTH");
    } else if left {
        return String::from("LEFT");
    } else if right {
        return String::from("RIGHT");
    } else {
        return String::from("NONE");
    }
}

fn main() {
    let mut ninput = String::new();
    io::stdin().read_line(&mut ninput).expect("Number of rack");
    let n = ninput.trim().parse::<usize>().expect("Number of rack");

    let mut pinput = String::new();
    io::stdin().read_line(&mut pinput).expect("Array");
    let mut _piter = pinput.split_whitespace();

    let mut p = Vec::new();
    for _ in 0..n {
        let tmp = _piter.next().unwrap().parse::<i64>().expect("Enter number");
        p.push(tmp);
    }

    let mut h = Vec::new();
    let mut hinput = String::new();
    io::stdin().read_line(&mut hinput).expect("Array");
    let mut _hiter = hinput.split_whitespace();

    for _ in 0..n {
        let tmp = _hiter.next().unwrap().parse::<i64>().expect("Enter number");
        h.push(tmp);
    }

    println!("{}", checkAll(n, h, p));
}
