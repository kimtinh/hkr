use std::io;

fn choose_pivot(left: usize, right: usize) -> usize {
    if left < right+1 {
        (left+right+1)/2
    } else {
        left
    }
}

fn partition(data: &mut Vec<f64>, left: usize, right: usize) -> usize {
    let pivotIndex: usize = choose_pivot(left, right);
    let pivotValue: f64 = data[pivotIndex];
    data.swap(pivotIndex, right);

    let mut storeIndex: usize = left;
    for i in left..right {
        if data[i] < pivotValue {
            data.swap(storeIndex, i);
            storeIndex += 1;
        }
    }

    data.swap(storeIndex, right);
    storeIndex
}

fn quicksort(data: &mut Vec<f64>, left: usize, right: usize) {
    if left < right && right < data.len() {
        let p: usize = partition(data, left, right);
        quicksort(data, left, p - 1);
        quicksort(data, p + 1, right);
    }
}

fn examRush(tm_size: usize, tm: &mut Vec<f64>, t: f64) -> u64 {
    let mut count = 0;
    let mut t2 = t;
    quicksort(tm, 0, tm_size - 1);

    for i in 0..tm_size {
        t2 = t2 - tm[i];
        if 0.0 <= t2 {
            count = count + 1;
        } else {
            return count;
        }
    }

    count
}

fn main() {
    let mut input = String::new();
    io::stdin().read_line(&mut input).expect("Enter 2 input");
    let mut _iter = input.split_whitespace();

    let tm_size = _iter
        .next()
        .unwrap()
        .parse::<usize>()
        .expect("Enter number");
    let t = _iter.next().unwrap().parse::<f64>().expect("Enter number");

    let mut tm = Vec::new();
    for _ in 0..tm_size {
        let mut cinput = String::new();
        io::stdin()
            .read_line(&mut cinput)
            .expect("Time to finish chapter");
        let tmp = cinput.trim().parse::<f64>().expect("Time to read chapter");
        tm.push(tmp);
    }

    println!("{}", examRush(tm_size, &mut tm, t));
}
