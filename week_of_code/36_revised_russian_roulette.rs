use std::io;

fn main() {
    let mut nkinput = String::new();
    io::stdin().read_line(&mut nkinput).expect("Enter number");
    let t = nkinput
        .trim()
        .parse::<usize>()
        .expect("Please enter number");

    let mut input = String::new();
    io::stdin().read_line(&mut input).expect("Enter array");
    let mut _iter = input.split_whitespace();

    let mut check = false;
    let (mut min, mut max) = (0, 0);
    for _ in 0..t {
        let tmp = _iter.next().unwrap().parse::<u64>().expect("Enter number");
        if tmp == 1 {
            max = max + 1;
            if check {
                check = false;
            } else {
                min = min + 1;
                check = true;
            }
        } else {
            check = false;
        }
    }

    println!("{} {}", min, max);
}