use std::io;

fn main() {
    let mut nkinput = String::new();
    io::stdin().read_line(&mut nkinput).expect("Enter number");
    let t = nkinput
        .trim()
        .parse::<usize>()
        .expect("Please enter number");

    let mut acids = Vec::new();
    for _ in 0..t {
        let mut input = String::new();
        io::stdin().read_line(&mut input).expect("Enter string");
        let acid = input
            .trim()
            .parse::<String>()
            .expect("Please enter a string");
        acids.push(acid);
    }

    for i in 0..t {
        let len = acids[i].chars().count();
        let char_vec: Vec<char> = acids[i].chars().collect();
        let mut check = 0;
        if len >= 2 {
            if char_vec[len - 2] == 'i' && char_vec[len - 1] == 'c' {
                check = 1;
                if len >= 7 && char_vec[0] == 'h' && char_vec[1] == 'y' && char_vec[2] == 'd'
                            && char_vec[3] == 'r' && char_vec[4] == 'o' {
                            check = 2;
                        }
                }
        }

        if check == 2 {
            println!("non-metal acid");
        } else if check == 1 {
            println!("polyatomic acid");
        } else {
            println!("not an acid");
        }
    }
}
