use std::io;

fn waysToGiveACheck(board: Vec<Vec<char>>) -> u64 {
    let mut count = 0;

    let mut pawn_j = Vec::new();

    let mut black: Vec<[usize; 2]> = Vec::new();
    let mut b_king: [usize; 2];
    let mut b_queen: [usize; 2];
    let mut b_knight: Vec<[usize; 2]> = Vec::new(); // ma
    let mut b_bishop: Vec<[usize; 2]> = Vec::new(); // tuong
    let mut b_rook: Vec<[usize; 2]> = Vec::new(); // xe
    let mut b_pawn: Vec<[usize; 2]> = Vec::new(); // tot

    let mut w_king: [usize; 2];
    let mut w_queen: [usize; 2];
    let mut w_knight: Vec<[usize; 2]> = Vec::new(); // ma
    let mut w_bishop: Vec<[usize; 2]> = Vec::new(); // tuong
    let mut w_rook: Vec<[usize; 2]> = Vec::new(); // xe
    let mut w_pawn: Vec<[usize; 2]> = Vec::new(); // tot

    let mut sharp: Vec<[usize; 2]> = Vec::new();

    for i in 0..8 {
        for j in 0..8 {
            match board[i][j] {
                'P' => {
                    w_pawn.push([i, j]);
                    if i == 1 {
                        pawn_j.push(j);
                    }
                }
                'K' => { w_king = [i, j]; }
                'Q' => { w_queen = [i, j]; }
                'N' => { w_knight.push([i, j]); }
                'B' => { w_bishop.push([i, j]); }
                'R' => { w_rook.push([i, j]); }
                '#' => { sharp.push([i, j]); }
                _ => { black.push([i, j]); }
            }
        }
    }

    count
}

fn main() {
    let mut nkinput = String::new();
    io::stdin().read_line(&mut nkinput).expect("Enter number");
    let t = nkinput
        .trim()
        .parse::<usize>()
        .expect("Please enter number");

    for _ in 0..t {
        let mut board = Vec::new();
        for _ in 0..8 {
            let mut input = String::new();
            io::stdin().read_line(&mut input).expect("Enter string");
            let line = input
                .trim()
                .parse::<String>()
                .expect("Please enter string");
            let line_chars: Vec<char> = line.chars().collect();
            board.push(line_chars);
        }
        println!("{}", waysToGiveACheck(board));
    }
}