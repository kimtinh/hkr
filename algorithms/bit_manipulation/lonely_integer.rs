use std::io;

fn quicksort(n: &Vec<u64>, lo: usize, hi: usize) -> Vec<u64> {
    let mut tmp = n.clone();

    let p = tmp[(lo + hi) / 2];
    let mut i = lo;
    let mut j = hi;

    while i < j {
        while tmp[i] < p {
            i = i + 1;
        }
        while tmp[j] > p {
            j = j - 1;
        }

        if i <= j {
            let a = tmp[i];
            tmp[i] = tmp[j];
            tmp[j] = a;
            i = i + 1;
            if j > 0 {
                j = j - 1;
            }
        }
    }
    if lo < j {
        tmp = quicksort(&tmp, lo, j);
    }
    if i < hi {
        tmp = quicksort(&tmp, i, hi);
    }

    tmp
}

fn main() {
    let mut nkinput = String::new();
    io::stdin().read_line(&mut nkinput).expect("Enter number");
    let t = nkinput
        .trim()
        .parse::<usize>()
        .expect("Please enter number");

    let mut input = String::new();
    io::stdin().read_line(&mut input).expect("Enter array");
    let mut _iter = input.split_whitespace();

    let mut n = Vec::new();
    let mut count = Vec::new();
    for _ in 0..t {
        let tmp = _iter.next().unwrap().parse::<u64>().expect("Enter number");
        n.push(tmp);
        count.push(1);
    }

    if t == 1 {
        println!("{}", n[0]);
    } else {
        n = quicksort(&n, 0, t - 1);

        if n[0] != n[1] {
            println!("{}", n[0]);
        } else if n[t - 1] != n[t - 2] {
            println!("{}", n[t - 1]);
        } else {
            let mut i = 1;
            while i < t - 1 {
                if n[i - 1] != n[i] && n[i] != n[i + 1] {
                    println!("{}", n[i]);
                    break;
                }
                i = i + 1;
            }
        }
    }
}
