use std::io;

fn main() {
    let mut input = String::new();
    io::stdin().read_line(&mut input).expect("Enter a year");
    let y = input.trim().parse::<i64>().expect("Please enter a year");

    let mut day;
    let mut month = 9;
    if y != 1918 {
        day = 13;
        if y < 1918 {
            if y % 4 == 0 {
                day = 12;
            }
        } else {
            if y % 400 == 0 || (y % 4 == 0 && y % 100 != 0) {
                day = 12;
            }
        }
    } else {
        day = 26;
        month = 9;
    }

    println!("{:>02}.{:>02}.{}", day, month, y);
}
