use std::io;

fn main() {
    let mut input = String::new();
    io::stdin()
        .read_line(&mut input)
        .expect("Enter number of stones");
    let n = input
        .trim()
        .parse::<u64>()
        .expect("Please number of stones");

    let mut sinput = String::new();
    io::stdin().read_line(&mut sinput).expect("Enter a string");
    let e = sinput.trim().parse::<String>().expect("Enter a string");
    let e0: Vec<char> = e.chars().collect();
    let mut gem_e: Vec<char> = Vec::new();
    for i in e0 {
        if !gem_e.contains(&i) {
            gem_e.push(i);
        }
    }

    for _ in 1..n {
        let mut siinput = String::new();
        io::stdin().read_line(&mut siinput).expect("Enter a string");
        let ei = siinput.trim().parse::<String>().expect("Enter a string");
        let elements: Vec<char> = ei.chars().collect();

        let mut t = gem_e.len();
        let mut i = 0;
        while i < t {
            if !elements.contains(&gem_e[i]) {
                gem_e.remove(i);
                t = t - 1;
            } else {
                i = i + 1;
            }
        }
    }

    println!("{}", gem_e.len());
}
